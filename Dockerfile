# hadolint ignore=DL3007
FROM docker:latest

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF
ARG TARGETPLATFORM

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="image-factory" \
    org.opencontainers.image.description="A toolkit to automatically publish image" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/image-factory" \
    org.opencontainers.image.source="https://gitlab.com/op_so/docker/image-factory" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

# hadolint ignore=DL3018
RUN apk --no-cache add ca-certificates curl cosign git jq skopeo syft \
    && sh -c "$(wget -qO - https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin \
    && mkdir -p "/root/.docker/cli-plugins/"

COPY pushrm-dist/$TARGETPLATFORM/docker-pushrm /root/.docker/cli-plugins/
RUN chmod +x "/root/.docker/cli-plugins/docker-pushrm"
