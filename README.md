# Docker Image Factory

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/docker/image-factory/badges/main/pipeline.svg)](https://gitlab.com/op_so/docker/image-factory/pipelines)

A toolkit to automatically sign, update and publish Docker images by comparing `SBOM` changes. It contains a reusable [GitLab CI template](https://gitlab.com/op_so/projects/gitlab-ci-templates#image-factory-image-factorygitlab-ciyml) and a specific Docker image with usefull tools:

- cosign,
- docker,
- docker-pushrm,
- jq,
- skopeo,
- syft,
- task.

The image is also automatically signed, updated and published:

- **lightweight** image based on Alpine,
- multiarch with support of **amd64** and **arm64**,
- **automatically** updated by comparing SBOM changes,
- image **signed** with [Cosign](https://github.com/sigstore/cosign),
- an **SBOM attestation** added using [Syft](https://github.com/anchore/syft),
- available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/docker/image-factory) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-informational?logo=docker&logoColor=white&style=flat-square)](https://hub.docker.com/r/jfxs/image-factory) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-informational?logo=docker&logoColor=white&style=flat-square)](https://quay.io/repository/ifxs/image-factory) The Quay.io registry.

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/image-factory/-/blob/main/Dockerfile) and contains:

--SBOM-TABLE--

Details are updated on [Dockerhub Overview page](https://hub.docker.com/r/jfxs/image-factory) when an image is published.

## Versioning

The Docker tag is defined by the Docker version used and an increment to differentiate build with the same Docker version:

```text
<docker_version>-<increment>
```

Example: 20.10.22-003

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the SBOM attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
- **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
